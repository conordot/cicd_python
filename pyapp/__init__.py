from flask import Flask
from flask_api import status
from pathlib import Path
import os
from .lib import file_reader as file_reader

# Below is based on the example defined in the Flask guide. Only difference is its serving content from a file.
def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'cicd_python.sqlite'),
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/health')
    def health_check():
        return 'OK', status.HTTP_200_OK

    @app.route('/file')
    def read_file():
        contents = file_reader.read_file("data/data.txt")
        return contents 

    return app
