import os
from pathlib import Path

def read_file(path):
    full_path = os.path.realpath(os.path.join(Path.cwd(), os.environ.get("FLASK_APP", "pyapp"), path))
    file = open(full_path, "r")
    if file.mode == 'r':
        return file.read()
